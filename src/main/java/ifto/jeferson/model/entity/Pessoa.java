package ifto.jeferson.model.entity;

public class Pessoa {
    private Integer id;
    private String nome;
    public Pessoa(){}
    public Pessoa(Integer id, String nome){
        this.id = id;
        this.nome = nome;
    }
    public Integer getId() {
        return id;
    }
    public String getNome() {
        return nome;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    @Override
    public String toString() {
        return "{ \"id\" : " + this.id + ", \"nome\" : \"" + this.nome + "\" }";
    }
}
