package ifto.jeferson.model.util;

import java.sql.Connection;

public interface ConexaoJDBC {
    public Connection criarConexao();
}
